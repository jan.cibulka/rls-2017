clc;close all;
clear all;
format short

acc = 4;


%nahrani lin modelu pro nove ventily
load('0304')
gamma1 = 0.3;
gamma2 = 0.4;

c1 = k1*T1/A1;
c2 = k2*T2/A2;
p = zpk('s');
F11 = (gamma1 *c1)/(p*T1+1);
F12 = ((1-gamma2)*c1)/((p*T1 + 1)*(p*T3 + 1));
F21 = ((1-gamma1)*c2)/((p*T2 + 1)*(p*T4 + 1));
F22 = (gamma2*c2)/(p*T2 + 1);
%trochu vypocitame zlomky
F = [F11 F12 ; F21 F22];

%1
% vypocet RGA relative gain array 
% Ks = lim p-> 0 (p F(p) I 1/p) = F(0)
% freqresp(sys,0)
%RGA = Ks *** Ks^{-T}

Ks = freqresp(F,0);
rga = Ks.*inv(Ks');

%protoze jsou vhodne (blizko 1) prvky na vedlejsi diagonale, je vhodna%volba parovani u1 -> y2 a naopak

sim('model_1');
%2 Staticke rozvazbeni
Ksd = [Ks(1,1) 0;0 Ks(2,2)];
% %D-rozvazbovaci kompenzator 2x2
D = inv(Ks)*Ksd;
sim('model_2');

%3 
%gamma = 0.6 0.7
%Fs -> rga
%nahrani lin modelu pro nove ventily
load('linearizace')
gamma1 = 0.6; % v tom filu jsou spatne ulozene
gamma2 = 0.7;

c1 = k1*T1/A1;
c2 = k2*T2/A2;
p = zpk('s');
F11 = (gamma1 *c1)/(p*T1+1);
F12 = ((1-gamma2)*c1)/((p*T1 + 1)*(p*T3 + 1));
F21 = ((1-gamma1)*c2)/((p*T2 + 1)*(p*T4 + 1));
F22 = (gamma2*c2)/(p*T2 + 1);
%trochu vypocitame zlomky
F = [F11 F12 ; F21 F22];
% vypocet RGA relative gain array 
% Ks = lim p-> 0 (p F(p) I 1/p) = F(0)
% freqresp(sys,0)
%RGA = Ks *** Ks^{-T}

Ks = freqresp(F,0);
rga_2 = Ks.*inv(Ks');
sim('model_1');
% 
%4
%interaktorova matice z minule semestralky
%F*D_2 = inv(ksi_1) * Kdz = [Kdz(1,1)/p+alfa 0 ;0 Kdz(2,2)/p+beta]
alfa = 0.025;
beta = 0.025;

ksi_2 = [p+alfa 0; 0 p+beta];
Kd2 = [alfa*gamma1*c1 0 ; 0 beta*gamma2*c2];

D2 = inv(ksi_2*F)*Kd2;
sim('model_4');
% 
% %5
% %D_3 = F^-1 * D = adj(F) /det F * D
% %pro D = detFs -> D_3 = adj(F)
% %matlab: adjoint
% %F*D_3 = [detF 0 ; 0 detF]
% %det nebere zpk, ale sym
% 
%State model
A_lin = [-(1/T1),0,(A3/(A1*T3)),0;0 -(1/T2),0,(A4/(A2*T4));0,0,-(1/T3),0;0,0,0,-(1/T4)];
B_lin = [(gamma1*k1)/A1,0;0,(gamma2*k2)/A2;0,((1-gamma2)*k2)/A3;((1-gamma1)*k1)/A4,0];
C_lin = [1,0,0,0;0,1,0,0];
D_lin = [0,0;0,0];
syms s

Fs = C_lin*inv(s*eye(4)-A_lin)*B_lin;

D3 = adjoint(Fs);
D3 = vpa(D3,4)

% D3 =
%  
% [                            1.834e14/(2.882e17*s + 4.905e15), -7.919e28/((7.206e16*s + 1.185e15)*(4.504e15*s + 4.029e15))]
% [ -3.468e28/((2.882e17*s + 4.905e15)*(1.126e15*s + 3.309e14)),                             3.93e13/(7.206e16*s + 1.185e15)]

s = zpk('s')

D3b = [1.834e14/(2.882e17*s + 4.905e15),                               -7.919e28/((7.206e16*s + 1.185e15)*(4.504e15*s + 4.029e15));
    -3.468e28/((2.882e17*s + 4.905e15)*(1.126e15*s + 3.309e14))     , 3.93e13/(7.206e16*s + 1.185e15)]

% 
% figure
% hold on
% grid on
% plot(simout.Time, simout.Data)
% plot(simout1.Time, simout1.Data)
% title('ukol 5 - dynamický regulator')
% legend('y_1','y_2')


%6
%A

Fs4 = [Kd2(1,1)/(s+alfa) ,0 ;0,Kd2(2,2)/(s+beta)];

F11 = Fs4(1,1)
F22 = Fs4(2,2)

Kp1 = 9.59;
Ki1 = 0.4417;

Kp2 = 22.92;
Ki2 = 0.6149;

sim('model_6')


%B
Fs5 = vpa(Fs,4)*adjoint(Fs)


