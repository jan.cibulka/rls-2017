clearvars -except nevykresluj;clc;close all;

g = 9.81;
gamma1 = 0.4;
gamma2 = 0.3;
A1 = 1.1;
A2 = 1.1;
A3 = 0.45;
A4 = 0.45;
a1 = 0.01;
a2 = 0.01;
a3 = 0.02;
a4 = 0.02;
k1 = 0.001;
k2 = 0.001;
h1 = 1.5;
h2 = 1.4;

h10 = 1.5;
h20 = 1.4;
h30 = 0.093118941471179;
h40 = 0.212362176660044;

T1 = A1/a1 * sqrt(2*h10/g);
T2 = A2/a2 * sqrt(2*h20/g);
T3 = A3/a3 * sqrt(2*h30/g);
T4 = A4/a4 * sqrt(2*h40/g);

A = [ -1/T1, 0, A3/(A1*T3),     0;
      0, -1/T2, 0,      A4/(A2*T4);
      0,     0,  -1/T3,          0;
      0,     0,      0,      -1/T4];
  
B = [ (gamma1 *k1)/A1, 0;
        0,  gamma2*k2/A2;
        0,  (1-gamma2)*k2/A3;
        (1-gamma1)*k1/A4, 0];
    
C = eye(4);
C_simple = [ 1 0 0 0; 0 1 0 0];
D = zeros(4,2);
D_simple = zeros(2,2);

[p_s, z_s] = pzmap(ss(A,B,C,D));
p = [-0.025 -0.025 -0.25, -0.21];

K = place(A,B,p);

x0 = [0.1, 0.1, 0.1, 0.1];

    

%% Rozsireni a Kompezace
%F s (0).K k = I 2x2
F = C_simple* inv(-A + B*K)*B; %  
K_komp = inv(F);


%% s integraci 
%
A_ext = [A , zeros(4,2);
         [-1 0 0 0;0 -1 0 0], zeros(2,2)];
B_ext = [B; zeros(2,2)];     
C_ext = [C_simple, zeros(2,2)];
D_ext = D;
K_ext = place(A_ext, B_ext, [p, -0.3226, -0.2136]);
Fs = ss(A-B*K, B, C, 0);
    
%% Rekonstructor
%

C_simple = [ 1 0 0 0
             0 1 0 0];
K_rek = place(A',C_simple', p);
L = K_rek';

A_rek = (A-L*C_simple);
B_rek = [ B L ];


%% Kalman filtering
%

Q_kalman = [ 0    0   0   0  ;
             0    0   0   0  ;
             0    0   0   0  ;
             0    0   0   0] ; % Process covariance
         
R_kalman = diag(0.01*ones(1,2));
N_kalman = 0;

kalman_sys = ss(A,B,C_simple,zeros(2,2));

% LQ regulator

lq_sys = ss(A_ext,B_ext,C_ext,zeros(2,2));

Q_lq = [ 40 40 0.2 0.2 3 3].*eye(6);

R_lq = [0.2 0
          0 0.2];

K_lq = lqr(lq_sys, Q_lq, R_lq);





