clc;close all;
clear all;
format short

acc = 4;
linearizace

c1 = k1*T1/A1;
c2 = k2*T2/A2;

p = zpk('s');

F11 = (gamma1 *c1)/(p*T1+1);
F12 = ((1-gamma2)*c1)/((p*T1 + 1)*(p*T3 + 1));
F21 = ((1-gamma1)*c2)/((p*T2 + 1)*(p*T4 + 1));
F22 = (gamma2*c2)/(p*T2 + 1);

%trochu vypocitame zlomky
F = [F11 F12 ; F21 F22];




magic = 1.3;
Treg = 250;
%ukol 3
alfa = 0.01*magic; %vyssi rychlejsi 
beta = alfa;
ksi_l = [p+alfa,0; 0,p+beta];
ksi_0 = [alfa,0; 0,beta];
Q = minreal(inv(ksi_l*F)*ksi_0);

%ukol 1
K = minreal(Q*inv(eye(2) - F*Q));

%ukol 2

omega_1 = 4.8*magic/Treg;
omega_2 = omega_1;
ny = 0.85;
ksi_2 = [p^2 + p*2*ny*omega_1+omega_1^2 ,0; 0,p^2 + p*2*ny*omega_2+omega_2^2 ];
ksi_20 = [omega_1^2 , 0; 0 , omega_2^2];

%S+T = 1 , S je HP, T je LP
%snizit vliv sumu, a nemodelovatelne vysoke frekvence
T_2 = [omega_1^2 / (p^2 + p*2*ny*omega_1+omega_1^2), 0 ; 0 , omega_2^2 / (p^2 + p*2*ny*omega_2+omega_2^2)]; %vyssi relativni rad
T = [alfa/(p + alfa) 0; 0 beta/(p + beta)]; %puvodni jednodussi


Q_2 =  minreal(inv(ksi_2*F)*ksi_20);
K_2 = minreal(Q_2*inv(eye(2) - F*Q_2));

figure;
hold on;
sigma(K);
sigma(K_2);
legend('K_1','K_2');

figure;
hold on;
sigma(T);
sigma(T_2);
legend('T_1','T_2');

rad_1  = minreal(ss(K));
rad_2  = minreal(ss(K_2));







