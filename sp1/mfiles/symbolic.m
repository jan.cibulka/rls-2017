clearvars -except vykresli;clc;close all;

%% Zadani
%

syms g h1 h2 h3 h4 a1 a2 a3 a4 A1 A2 A3 A4 k1 k2 u1 u2 gamma1 gamma2 h30 h40

u1 = a4*sqrt(2*g*h4) / ( (1 - gamma1 ) * k1) 
u2 = a3*sqrt(2*g*h3) / ( (1 - gamma2 ) * k2) 

eq1 = -(a1/A1) * sqrt(2*h1*g) + (a3/A1)*sqrt(2*h3*g) + (gamma1 * k1 *u1)/A1 == 0
eq2 = -(a2/A2) * sqrt(2*h2*g) + (a4/A2)*sqrt(2*h4*g) + (gamma2 * k2 *u2)/A2 == 0

sol1 = solve([eq1, eq2],[h3, h4])

h3 = sol1.h3
h4 = sol1.h4

%% naplnim konstanty
%
g = 9.81;
gamma1 = 0.4;
gamma2 = 0.3;
A1 = 1.1;
A2 = 1.1;
A3 = 0.45;
A4 = 0.45;
a1 = 0.01;
a2 = 0.01;
a3 = 0.02;
a4 = 0.02;
k1 = 0.001;
k2 = 0.001;
h1 = 1.5;
h2 = 1.4;

%% Vyhodnoceni
u10_s = eval(subs(subs(u1)))
u20_s = eval(subs(subs(u2)))
h30_s = eval(subs(subs(h3)))
h40_s = eval(subs(subs(h4)))




