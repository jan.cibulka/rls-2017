symbolic

format shortG

%eq3 = -(a3/A4) * sqrt( 2* h3 * g) + ( (1 - gamma2) * k2 * u2  ) / A3 == 0
%eq4 = -(a3/A4) * sqrt( 2* h4 * g) + ( (1 - gamma1) * k2 * u2  ) / A4 == 0


T1 = A1/a1 * sqrt(2*h1/g);
T2 = A2/a2 * sqrt(2*h2/g);
T3 = A3/a3 * sqrt(2*h30_s/g);
T4 = A4/a4 * sqrt(2*h40_s/g);

A_lin = [ -1/T1, 0, A3/(A1*T3),     0;
      0, -1/T2, 0,      A4/(A2*T4);
      0,     0,  -1/T3,          0;
      0,     0,      0,      -1/T4];
  
 B_lin = [ (gamma1 *k1)/A1, 0;
        0,  gamma2*k2/A2;
        0,  (1-gamma2)*k2/A3;
        (1-gamma1)*k1/A4, 0];
    
C_lin = eye(4);
D_lin = zeros(4,2);

if(exist('vykresli','var')) % Promena existuje, proto se kod vykona
    sim('model1')
    figure(1)
    plot(simout);
    legend('h_1 ns','h_2 ns', 'h_1 lin', 'h_2 lin')
    title('Porovnani hladin')

    figure(2)
    plot(simout1);
    legend('h_3 ns','h_4 ns', 'h_3 lin', 'h_4 lin')
    title('Porovnani hladin')

    figure(3)
    plot(simout2);
    legend('u_1','u_2')
    title('Zmena vstupu')
end


%% Controler
% in simulink P + I * (1/s) = K * (1 + 1/(Ti * s)))
% bod 8

K = 0.1;
Ki = 0.5; % otazka jestli Ki je K/Ti nebo jestli Ki bylo mysleno Ti :O what?

P_reg = K;
I_reg = Ki;