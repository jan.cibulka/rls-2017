clc;clear all;format long
  
%z linearizace
u1_s = 68.04;
u2_s = 38.619;
h3_s = 0.093119;
h4_s =0.21236 ;

%ze zadani
g = 9.81;
gamma1 = 0.4;
gamma2 = 0.3;
A1 = 1.1;
A2 = 1.1;
A3 = 0.45;
A4 = 0.45;
a1 = 0.01;
a2 = 0.01;
a3 = 0.02;
a4 = 0.02;
k1 = 0.001;
k2 = 0.001;
h1 = 1.5;
h2 = 1.4;

%pripravne vypocty
T1 = A1/a1 * sqrt(2*h1/g);
T2 = A2/a2 * sqrt(2*h2/g);
T3 = A3/a3 * sqrt(2*h3_s/g);
T4 = A4/a4 * sqrt(2*h4_s/g);

c1 = k1*T1/A1;
c2 = k2*T2/A2;

syms p;
F11 = (gamma1 *c1)/(p*T1+1);
F12 = ((1-gamma2)*c1)/((p*T1 + 1)*(p*T3 + 1));
F21 = ((1-gamma1)*c2)/((p*T2 + 1)*(p*T4 + 1));
F22 = (gamma2*c2)/(p*T2 + 1);

%trochu vypocitame zlomky
F = vpa([F11 F12 ; F21 F22],4);

%%LEVY MATICOVY ZLOMEK
Flsd = vpa([(p*T1 +1)*(p*T3 + 1) 0 ; 0 (p*T2 + 1)*(p*T4 + 1)],4);
Flsn = vpa([gamma1*c1*(p*T3 + 1) (1-gamma2)*c1 ; (1-gamma1)*c2 gamma2*c2*(p*T4 + 1)],4);

%% PRAVY MATICOVY ZLOMEK
Fpsd = vpa([gamma1*c1*(p*T2 + 1)*(p*T4 + 1) (1-gamma2)*c1*(p*T2+1); (1-gamma1)*c2*(p*T1+1) gamma2*c2*(p*T1+1)*(p*T3+1)],4);
Fpsn = vpa([(p*T1+1)*(p*T2+1)*(p*T4+1) 0 ; 0 (p*T1+1)*(p*T2+1)*(p*T3+1)],4);

%Poly
A = [-0.016439            0      0.13196            0;
            0    -0.017016            0     0.087381;
            0            0     -0.32257            0;
            0            0            0      -0.2136 ];
B = [0.00036364            0;
            0   0.00027273;
            0    0.0015556;
    0.0013333            0 	];
C = [1 0 0 0 ; 0 1 0 0];
poly = eig(A);
D = zeros(2);

%Nuly
syms p ;
h = (1-gamma1)*(1-gamma2)/(gamma1*gamma2);
detNuly = T3*T4*p^2 + (T3+T4)*p+ 1-h == 0;
nuly = vpa(solve(detNuly,p),4);

rosenbrock = [(p*eye(4) - A) -B; C zeros(2)];
detRose = det(rosenbrock);
nulyRose = vpa(solve(det(rosenbrock),p),4);


%SMERY NUL
%1
nula1 = nuly(2);
Fz1 = subs(F,p,nula1);

Fz1 = vpa(Fz1,8);
syms u1 u2;

vyrazy = Fz1*[u1 ; u2];
vyjadreni1 = solve(vyrazy(1) == 0, u2);
smerVstup1 = [1 ; vyjadreni1/u1];

syms y1 y2;
vyrazy =  [y1  y2] *Fz1;
vyjadreni1 = solve(vyrazy(1) == 0, y2);
smerVystup1 = [1 ; vyjadreni1/y1];

%% Smith-McMillan
syms p;
vpa([1/((p*T1 +1)*(p*T2 + 1)*(p*T3 + 1)*(p*T4 + 1)), 0; 0, c1*c2*(gamma1*gamma2*(p*T3 + 1)*(p*T4 + 1)-(1-gamma1)*(1-gamma2))],4);
polySM = vpa(solve((p*T1 +1)*(p*T2 + 1)*(p*T3 + 1)*(p*T4 + 1),p),4);
nulySM = vpa(solve(c1*c2*(gamma1*gamma2*(p*T3 + 1)*(p*T4 + 1)-(1-gamma1)*(1-gamma2)),p),4);

%%MIMO stabilita
Frd = [ p , 0 ;0  , p];
Frn = [(0.1*p + 0.5), 0 ; 0 , (0.1*p + 0.5)];

vpa(solve(det(Flsd*Frd + Flsn*Frn) == 0, p),4)




 
