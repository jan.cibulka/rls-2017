clearvars -except nevykresluj;clc;close all;

g = 9.81;
gamma1 = 0.6;
gamma2 = 0.7;
A1 = 1.1;
A2 = 1.1;
A3 = 0.45;
A4 = 0.45;
a1 = 0.01;
a2 = 0.01;
a3 = 0.02;
a4 = 0.02;
k1 = 0.001;
k2 = 0.001;
h1 = 1.5;
h2 = 1.4;

h10 = 1.5;
h20 = 1.4;
h30 = 0.093118941471179;
h40 = 0.212362176660044;

T1 = A1/a1 * sqrt(2*h10/g);
T2 = A2/a2 * sqrt(2*h20/g);
T3 = A3/a3 * sqrt(2*h30/g);
T4 = A4/a4 * sqrt(2*h40/g);

A = [ -1/T1, 0, A3/(A1*T3),     0;
      0, -1/T2, 0,      A4/(A2*T4);
      0,     0,  -1/T3,          0;
      0,     0,      0,      -1/T4];
  
B = [ (gamma1 *k1)/A1, 0;
        0,  gamma2*k2/A2;
        0,  (1-gamma2)*k2/A3;
        (1-gamma1)*k1/A4, 0];
    
C = eye(4);
C_simple = [ 1 0 0 0; 0 1 0 0];
D = zeros(4,2);
D_simple = zeros(2,2);

%% Interoktorova matice
s = zpk('s');
c = 1; % muze byt cokoliv > 0
theta_r = [s+c, 0;
             0, s+c];
%% Jak chceme aby se system choval
%
c1 = k1*T1/A1;
c2 = k2*T2/A2;

F11 = (gamma1 *c1)/(s*T1+1);
F12 = ((1-gamma2)*c1)/((s*T1 + 1)*(s*T3 + 1));
F21 = ((1-gamma1)*c2)/((s*T2 + 1)*(s*T4 + 1));
F22 = (gamma2*c2)/(s*T2 + 1);

%trochu vypocitame zlomky
Fs = [F11, F12 ; F21, F22];

tau = 80;
T_star = [1/(tau*s + 1), 0
                     0, 1/(tau*s +1)];
G = Fs*theta_r;
% Rozsireny system
P = [ T_star, -G;
      eye(2), -G];

  
%% 
% 1. krok
P_1 = ss(P);

% 2. krok
P_2 = minreal(P_1);

% 3. krok make two inputs two outputs, aby matlab vedel
n_u = 2; % Z formulace problemu
n_y = 2; % Z formulace problemu

P_3 = mktito(P_2, n_u, n_y);


%% H2 optimalizace
[K_reg,CL,GAM,INFO] = h2syn(P_3);


K_bar = theta_r * K_reg;


% Q parametrizace, dodatek
alpha_q = 1;
beta_q = 1;
theta_rq = T_star*[alpha_q/(s+alpha_q), 0;
             0, beta_q/(s+beta_q)];

% Pokud je vnitrne stabilni plant Fs
%   Vynásobíme Fs levou interactorovou a udělam inverzi 

Q = inv(Fs) * theta_rq;
K_regq = Q/(eye(2) - Fs*Q);

sim('model1');

%% Plot
%

figure(1);
hold on
plot(simout);
title('H2 optimal, Q-param, T*')
legend('Optimální zpětnovazební h30', ...
       'Optimální zpětnovazební h40', ...
       'Q-parametrizace h30', ...
       'Q-parametrizace h40', ...
       'T* h30', ...
       'T* h40');
   
figure(2)
hold on 
plot(simout1)
title('Řízení')


         
         