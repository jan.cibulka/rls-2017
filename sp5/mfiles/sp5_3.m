clearvars -except nevykresluj;clc;close all;

g = 9.81;
gamma1 = 0.6;
gamma2 = 0.7;
A1 = 1.1;
A2 = 1.1;
A3 = 0.45;
A4 = 0.45;
a1 = 0.01;
a2 = 0.01;
a3 = 0.02;
a4 = 0.02;
k1 = 0.001;
k2 = 0.001;
h1 = 1.5;
h2 = 1.4;

h10 = 1.5;
h20 = 1.4;
h30 = 0.093118941471179;
h40 = 0.212362176660044;

T1 = A1/a1 * sqrt(2*h10/g);
T2 = A2/a2 * sqrt(2*h20/g);
T3 = A3/a3 * sqrt(2*h30/g);
T4 = A4/a4 * sqrt(2*h40/g);

A = [ -1/T1, 0, A3/(A1*T3),     0;
      0, -1/T2, 0,      A4/(A2*T4);
      0,     0,  -1/T3,          0;
      0,     0,      0,      -1/T4];
  
B = [ (gamma1 *k1)/A1, 0;
        0,  gamma2*k2/A2;
        0,  (1-gamma2)*k2/A3;
        (1-gamma1)*k1/A4, 0];
    
C = eye(4);
C_simple = [ 1 0 0 0; 0 1 0 0];
D = zeros(4,2);
D_simple = zeros(2,2);

s = zpk('s');

%% Sys
c1 = k1*T1/A1;
c2 = k2*T2/A2;

F11 = (gamma1 *c1)/(s*T1+1);
F12 = ((1-gamma2)*c1)/((s*T1 + 1)*(s*T3 + 1));
F21 = ((1-gamma1)*c2)/((s*T2 + 1)*(s*T4 + 1));
F22 = (gamma2*c2)/(s*T2 + 1);

%trochu vypocitame zlomky
Fs = [F11, F12 ; F21, F22];


%% S/KS problem, 2 varianta
% 
% zvolime M, Am omega_b, omega_1
% W2 = konst.

M = 2; 
omega_b = 1e-2; % bandwidth, kolem setiny, abysme dosáhli 250 [s]
omega_b_boosted = 5e-2; % Upravena 
A = 1e-4; 
c= 1.9*1e-2;
G = Fs;



w1 = ( (s/M) + omega_b )/( s + omega_b*A ); 
w1_boosted = ((s/M) + omega_b_boosted )/( s + omega_b_boosted*A );
W1 = [w1_boosted, 0; 0, w1];

%W2 = c*eye(2); ne moc cool
%% lead lag, high pass
%{
A2=2e-2;
M2=0.2;
omega_b2 = 1e-2;
w2 = ( s + omega_b2*A2 ) / ( (s/M2) + omega_b )
%}

W2 = makeweight(1e-3,10^(0.5),10^(50/20)) * eye(2);



P = [W1, W1*G;
     zeros(2), -W2;
     -eye(2), -G];

[K,CL,GAM,INFO] = hinfsyn(minreal(P), 2, 2);
S = inv(eye(2) + G*K);
T = eye(2) - S;

sim('model3')
figure(1)
hold on
plot(simout)
title('Výstup regulace nezrychlené systému')
legend('h_1','h_2', 'sp1', 'sp2')

figure(2)
hold on 
plot(simout1)
title('Akční zásahy nezrychleného systému')
legend('u_{r1}', 'u_{r2}')








