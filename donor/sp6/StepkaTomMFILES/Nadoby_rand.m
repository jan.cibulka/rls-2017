function [c1,c2,c3,c4,T1,T2,T3,T4]=Nadoby_rand(h10,h20)
%GENEROVANI PARAMETRU KADINEK PRO RUZNE VYSKY HLADIN

%nastaveni­ parametru
A1 = 1.2;
A2 = 1.2;
A3 = 0.5;
A4 = 0.5;
a1 = 0.01;
a2 = 0.01;
a3 = 0.02;
a4 = 0.02;
gama1 = 0.6;
gama2 = 0.7;
k1 = 0.001;
k2 = 0.001;
g = 9.81;
%pracovni bod
%h10 = 1.5;
%h20 = 1.2;

% dopocitani hladin a napeti
p1 = gama1/(1-gama1);
p2 = gama2/(1-gama2);
t10 = a1*sqrt(2*g*h10);
t20 = a2*sqrt(2*g*h20);

h40 = (((t20 - (t10*p2))/(a4*(1-(p1*p2))))^2)/(2*g);
t40 = a4*sqrt(2*g*h40);
h30 = (((t10 - (t40*p1))/a3)^2)/(2*g);
t30 = a3*sqrt(2*g*h30);
u10 = t40/((1-gama1)*k1);
u20 = t30/((1-gama2)*k2);

%casove konstanty pro stavovy model
T1 = (A1/a1)*sqrt((2*h10)/g);
T2 = (A2/a2)*sqrt((2*h20)/g);
T3 = (A3/a3)*sqrt((2*h30)/g);
T4 = (A4/a4)*sqrt((2*h40)/g);

A = [-(1/T1) 0 A3/(A1*T3) 0;
     0 -(1/T2) 0 A4/(A2*T4);
     0 0 -(1/T3) 0;
     0 0 0 -(1/T4)];
 B = [(gama1*k1)/A1 0;
      0 (gama2*k2)/A2;
      0 ((1-gama2)*k2)/A3;
      ((1-gama1)*k1)/A4 0];
 C = [1 0 0 0;
      0 1 0 0];
D = [0 0;0 0];

% zesileni pro integratory
g11 = (a1/A1)*sqrt(g/(2*h10));
g12 = (a3/A1)*sqrt(g/(2*h30));
g13 = (gama1*k1)/A1;

g21 = (a2/A2)*sqrt(g/(2*h20));
g22 = (a4/A2)*sqrt(g/(2*h40));
g23 = (gama2*k2)/A2;

g31 = (a3/A3)*sqrt(g/(2*h30));
g32 = ((1-gama2)*k2)/A3;

g41 = (a4/A4)*sqrt(g/(2*h40));
g42 = ((1-gama1)*k1)/A4;

c1=(gama1*k1)/A1;
c2=((1-gama2)*k2)/A1;
c3=((1-gama1)*k2)/A2;
c4=(gama2*k2)/A2;

end