close all
clear all

load('model.mat')

s = zpk('s')
%NS
S = inv(eye(2)+Pn*K)
poly_S = pole(S)

w1 = (0.5*(s+0.02))/(s+1e-6);
W1 = w1*eye(2);

%NP
figure(1);sigma(W1*S)

figure(2);sigma(W1*S,inv(W1))

norm(W1*S,inf)

%RS
% lze pouzit vypoctene r_0 z 1. ukolu
w=linspace(1e-3,500,2000); %frekvence pro vysetreni multiplik. perturbace
w=logspace(-3,3,2000)

load('r_0_file.mat')
T = eye(2) - S;

singularni_cisla = max(sigma(T,w));
RS=zeros(1,length(r_0));

for i = 1:1:length(r_0)
    RS(i) = r_0(i)*singularni_cisla(i);
end

figure(3);plot(RS)

% 
% figure(4)
% ns = 200
% h10_l=0.2;
% h10_h=3;
% h20_l=0.2;
% h20_h=3;
% h10_rand = h10_l + (h10_h-h10_l).*rand(ns,1);
% h20_rand = h20_l + (h20_h-h20_l).*rand(ns,1);
% 

% for i=1:ns
%     [c1,c2,c3,c4,T1(i),T2(i),T3(i),T4(i)]=Nadoby_rand(h10_rand(i),h20_rand(i));
%    
%     
%     
%     P11=c1*T1(i)/(T1(i)*s+1);
%     P12=c2*T1(i)/((T1(i)*s+1)*(T3(i)*s+1));
%     P21=c3*T2(i)/((T2(i)*s+1)*(T4(i)*s+1));
%     P22=c4*T2(i)/(T2(i)*s+1);
%       
%     P=[P11 P12;P21 P22];
%     
%     Stest = inv(eye(2)+P*K);
%     Ttest = eye(2) - Stest;
%     
%     singularni_cisla = max(sigma(Ttest,w));
%     RS=zeros(1,length(r_0));
% 
%     for j = 1:1:length(r_0)
%          RS(j) = r_0(j)*singularni_cisla(j);
%     end
%     u = RS + max(sigma(W1*S,w));
%     plot(u)
%     hold on;
%      
% end;


%RP

% u = RS + max(sigma(W1*S,w))
% 
% figure(5);plot(u)


% ukol 5


[K_inf,CL,GAM,INFO] = mixsyn(Pn,W1,[],0.7)

% S_inf = inv(eye(2)+Pn*K_inf)
% poly_S_inf = pole(S_inf)
% 
% T_inf = eye(2) - S_inf;
% 
% % RS
% 
% singularni_cisla_inf = max(sigma(T_inf,w));
% RS_inf=zeros(1,length(r_0));
% 
% for i = 1:1:length(r_0)
%     RS_inf(i) = 0.7*singularni_cisla_inf(i);
% end
% 
% figure(6)
% ns = 200
% h10_l=0.2;
% h10_h=3;
% h20_l=0.2;
% h20_h=3;
% h10_rand = h10_l + (h10_h-h10_l).*rand(ns,1);
% h20_rand = h20_l + (h20_h-h20_l).*rand(ns,1);
% 
% 
% for i=1:ns
%     [c1,c2,c3,c4,T1(i),T2(i),T3(i),T4(i)]=Nadoby_rand(h10_rand(i),h20_rand(i));
%    
%     
%     
%     P11=c1*T1(i)/(T1(i)*s+1);
%     P12=c2*T1(i)/((T1(i)*s+1)*(T3(i)*s+1));
%     P21=c3*T2(i)/((T2(i)*s+1)*(T4(i)*s+1));
%     P22=c4*T2(i)/(T2(i)*s+1);
%       
%     P=[P11 P12;P21 P22];
%     
%     S_inf = inv(eye(2)+P*K_inf);
%     T_inf = eye(2) - S_inf;
%     
%     singularni_cisla_inf = max(sigma(T_inf,w));
%     RS_inf=zeros(1,length(r_0));
% 
%     for j = 1:1:length(r_0)
%          RS_inf(j) = 0.7*singularni_cisla_inf(j);
%     end
%     %u = RS_inf + max(sigma(W1*S_inf,w));
%     plot(RS_inf)
%     hold on;
%      
% end;

%figure(5);plot(RS_inf)

% figure(6)
% hold on
% sigma(inv(W1))
% sigma(T_inf)
% sigma(S_inf)
% sigma(K_inf)
% hold off
% 
% GAM - 1/sqrt(2)

%ukol 6

kn1 = ureal('kn1',1,'Percentage',50)
kn2 = ureal('kn2',1,'Percentage',50)

tn1 = ureal('tn1',2,'Range',[1.5,2.5])
tn2 = ureal('tn2',2,'Range',[1.5,2.5])

Pu1 = kn1/(tn1*s + 1)
Pu2 = kn2/(tn2*s + 1)

P = Pn * [Pu1, 0; 0, Pu2]

L = P*K_inf
SS = inv(eye(2)+L)
[STABMARG,WCU] = robstab(SS)
[WCG,WCU] = wcgain(W1*SS)

%ukol 7

PP = -[W1 ;eye(2)]*[eye(2) P]
 
[KK,CLP,BND] = dksyn(PP,2,2)
 
minreal(KK)
hsvd(KK)
KKK = balred(KK,7)
SSS = inv(eye(2)+P*KKK) 
figure(7);sigma(W1*SSS)

[WCG1,WCU1] = wcgain(W1*SSS)