
%RLS - SP1
%==========================================================================
%==========================================================================
clear all;
clc;
close all;
%Nonlinar model
%==========================================================================

syms a1 a2 a3 a4 A1 A2 A3 A4 g h1 h2 h3 h4 k1 k2 gamma1 gamma2

u1 = (a4*sqrt(2*g*h4))/((1-gamma1)*k1);
u2 = (a3*sqrt(2*g*h3))/((1-gamma2)*k2);

equation1 = -((a1/A1)*sqrt(2*g*h1)) + ((a3/A1)*sqrt(2*g*h3)) + ((gamma1*k1*u1)/(A1));
equation2 = -((a2/A2)*sqrt(2*g*h2)) + ((a4/A2)*sqrt(2*g*h4)) + ((gamma2*k2*u2)/(A2));

[H3 H4] = solve(equation1==0, equation2 ==0, h3,h4);

%{
pretty(H3);
pretty(H4);
%}

A1 = 1.1;
A2 = 1.1;
A3 = 0.45;
A4 = 0.45;
a1 = 0.01;
a2 = 0.01;
a3 = 0.02;
a4 = 0.02;
gamma1 = 0.6;
gamma2 = 0.7;
k1 = 0.001;
k2 = 0.001;
g = 9.81;
h1 = 1.55;
h2 = 1.45;

H30 = eval(H3);
H40 = eval(H4);

h3 = H30;
h4 = H40;

u10 = eval(u1);
u20 = eval(u2);

%equation3 = -((a3/A3)*sqrt(2*g*h3)) + (((1-gamma2)*k2*u20)/(A3));
%equation4 = -((a4/A4)*sqrt(2*g*h4)) + (((1-gamma1)*k1*u10)/(A4));



%Linear model
%==========================================================================

%Time constants
T1 = (A1/a1)*sqrt((2*h1)/g);
T2 = (A2/a2)*sqrt((2*h2)/g);
T3 = (A3/a3)*sqrt((2*h3)/g);
T4 = (A4/a4)*sqrt((2*h4)/g);

%State model
A = [-(1/T1),0,(A3/(A1*T3)),0;0 -(1/T2),0,(A4/(A2*T4));0,0,-(1/T3),0;0,0,0,-(1/T4)];
B = [(gamma1*k1)/A1,0;0,(gamma2*k2)/A2;0,((1-gamma2)*k2)/A3;((1-gamma1)*k1)/A4,0];
C = [1,0,0,0;0,1,0,0];
D = [0,0;0,0];

s = zpk('s')

Fs = C*inv(s*eye(4)-A)*B;

alpha = 0.05;
beta = 0.03;
c1 = k1*T1/A1;
c2 = k2*T2/A2;

Kd2 = [alpha*gamma1*c1 0;0 beta*gamma2*c2];
xi = [s+alpha, 0;0,s+beta];

D2 = inv(xi*Fs)*Kd2;
D2 = minreal(D2)
        
Kp = 32.4614216026751;
Ki = 2.1030958543319;
       
step_1 = 0.1;
step_2 = 0.1;

sim('Semestralka_4_regulace_model_ukol_4.slx')
 
         
figure(1)
hold on
plot(time,vystup_2(:,1))
plot(time,vystup_2(:,2))
xlabel('cas [s]')
ylabel('hladina [m]')
legend('h_1','h_2')
hold off


                     
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        







