
%RLS - SP1
%==========================================================================
%==========================================================================
clear all;
clc;
close all;
%Nonlinar model
%==========================================================================

syms a1 a2 a3 a4 A1 A2 A3 A4 g h1 h2 h3 h4 k1 k2 gamma1 gamma2

u1 = (a4*sqrt(2*g*h4))/((1-gamma1)*k1);
u2 = (a3*sqrt(2*g*h3))/((1-gamma2)*k2);

equation1 = -((a1/A1)*sqrt(2*g*h1)) + ((a3/A1)*sqrt(2*g*h3)) + ((gamma1*k1*u1)/(A1));
equation2 = -((a2/A2)*sqrt(2*g*h2)) + ((a4/A2)*sqrt(2*g*h4)) + ((gamma2*k2*u2)/(A2));

[H3 H4] = solve(equation1==0, equation2 ==0, h3,h4);

%{
pretty(H3);
pretty(H4);
%}

A1 = 1.1;
A2 = 1.1;
A3 = 0.45;
A4 = 0.45;
a1 = 0.01;
a2 = 0.01;
a3 = 0.02;
a4 = 0.02;
gamma1 = 0.3;
gamma2 = 0.4;
k1 = 0.001;
k2 = 0.001;
g = 9.81;
h1 = 1.55;
h2 = 1.45;

H30 = eval(H3);
H40 = eval(H4);

h3 = H30;
h4 = H40;

u10 = eval(u1);
u20 = eval(u2);

%equation3 = -((a3/A3)*sqrt(2*g*h3)) + (((1-gamma2)*k2*u20)/(A3));
%equation4 = -((a4/A4)*sqrt(2*g*h4)) + (((1-gamma1)*k1*u10)/(A4));



%Linear model
%==========================================================================

%Time constants
T1 = (A1/a1)*sqrt((2*h1)/g);
T2 = (A2/a2)*sqrt((2*h2)/g);
T3 = (A3/a3)*sqrt((2*h3)/g);
T4 = (A4/a4)*sqrt((2*h4)/g);

%State model
A = [-(1/T1),0,(A3/(A1*T3)),0;0 -(1/T2),0,(A4/(A2*T4));0,0,-(1/T3),0;0,0,0,-(1/T4)];
B = [(gamma1*k1)/A1,0;0,(gamma2*k2)/A2;0,((1-gamma2)*k2)/A3;((1-gamma1)*k1)/A4,0];
C = [1,0,0,0;0,1,0,0];
D = [0,0;0,0];

sym s
zpk('s')

Fs_0 = C*inv(0*eye(4)-A)*B

RGA = Fs_0.*inv(Fs_0)' %zaporna diagonala - nevhodny par    
                       %prvky blizke jednicce jsou vhodne - vhodne zapojit
                       %regulatory proti sobe
% pouzit model z 1
        
Kp = 32.4614216026751;
Ki = 2.1030958543319;
        
Ksd = [Fs_0(1,1) 0;0 Fs_0(2,2)]
D = inv(Fs_0)*Ksd
        
        
step_1 = 0.1;
step_2 = 0.1;

sim('Semestralka_4_regulace_model.slx')
% 
% figure(1)
% hold on
% plot(time,rizeni_1(:,1))
% plot(time,rizeni_1(:,2))
% xlabel('cas [s]')
% ylabel('akcni zasah')
% legend('u_1','u_2')
% hold off
%         
% figure(2)
% hold on
% plot(time,vystup_1(:,1))
% plot(time,vystup_1(:,2))
% xlabel('cas [s]')
% ylabel('hladina [m]')
% legend('h_1','h_2')
% hold off

sim('Semestralka_4_regulace_model_ukol_2.slx')

% figure(1)
% hold on
% plot(time,rizeni_2(:,1))
% plot(time,rizeni_2(:,2))
% xlabel('cas [s]')
% ylabel('akcni zasah')
% legend('u_1','u_2')
% hold off
%         
% figure(2)
% hold on
% plot(time,vystup_2(:,1))
% plot(time,vystup_2(:,2))
% xlabel('cas [s]')
% ylabel('hladina [m]')
% legend('h_1','h_2')
% hold off
             
figure(1)
hold on
plot(time,rizeni_1(:,1))
plot(time,rizeni_1(:,2))
plot(time,rizeni_2(:,1))
plot(time,rizeni_2(:,2))
xlabel('cas [s]')
ylabel('akcni zasah')
legend('1 u_1','1 u_2','2 u_1','2 u_2')
hold off
        
figure(2)
hold on
plot(time,vystup_1(:,1))
plot(time,vystup_1(:,2))
plot(time,vystup_2(:,1))
plot(time,vystup_2(:,2))
xlabel('cas [s]')
ylabel('hladina [m]')
legend('1 h_1','1 h_2','2 h_1','2 h_2')
hold off
                     
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        







