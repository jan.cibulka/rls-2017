
%RLS - SP5
%==========================================================================
%==========================================================================
clear all;
clc;
close all;
%Nonlinar model
%==========================================================================

syms a1 a2 a3 a4 A1 A2 A3 A4 g h1 h2 h3 h4 k1 k2 gamma1 gamma2

u1 = (a4*sqrt(2*g*h4))/((1-gamma1)*k1);
u2 = (a3*sqrt(2*g*h3))/((1-gamma2)*k2);

equation1 = -((a1/A1)*sqrt(2*g*h1)) + ((a3/A1)*sqrt(2*g*h3)) + ((gamma1*k1*u1)/(A1));
equation2 = -((a2/A2)*sqrt(2*g*h2)) + ((a4/A2)*sqrt(2*g*h4)) + ((gamma2*k2*u2)/(A2));

[H3 H4] = solve(equation1==0, equation2 ==0, h3,h4);

A1 = 1.1;
A2 = 1.1;
A3 = 0.45;
A4 = 0.45;
a1 = 0.01;
a2 = 0.01;
a3 = 0.02;
a4 = 0.02;
gamma1 = 0.6;
gamma2 = 0.7;
k1 = 0.001;
k2 = 0.001;
g = 9.81;
h1 = 1.55;
h2 = 1.45;

H30 = eval(H3);
H40 = eval(H4);

h3 = H30;
h4 = H40;

u10 = eval(u1);
u20 = eval(u2);

%Linear model
%==========================================================================

%Time constants
T1 = (A1/a1)*sqrt((2*h1)/g);
T2 = (A2/a2)*sqrt((2*h2)/g);
T3 = (A3/a3)*sqrt((2*h3)/g);
T4 = (A4/a4)*sqrt((2*h4)/g);

%State model
A = [-(1/T1),0,(A3/(A1*T3)),0;0 -(1/T2),0,(A4/(A2*T4));0,0,-(1/T3),0;0,0,0,-(1/T4)];
B = [(gamma1*k1)/A1,0;0,(gamma2*k2)/A2;0,((1-gamma2)*k2)/A3;((1-gamma1)*k1)/A4,0];
C = [1,0,0,0;0,1,0,0];
D = [0,0;0,0];
alpha = 0.05;
beta = 0.03;

s = zpk('s')

Fs = C*inv(s*eye(4)-A)*B;
T_star = [1/(80*s+1) 0; 0 1/(80*s+1)];

ksi_t = [s+1, 0;0, s+1]; %prava interaktorova matice s libovolnym parametrem - jen pomucka
G = Fs*ksi_t;

P0 = [T_star -G; eye(2) -G];
P1 = ss(P0);
P2 = minreal(P1);
P3 = mktito(P2,2,2);
[K, CL, Gam, Info] = h2syn(P3);   %h2 norma
%gam - nula - podarila se shoda
%d12 does not have full column rank - nesplneni vsech podminek zobecneneho
%modelu

%Q = [0.1129032258e3 / (s + alpha) / (0.2479996376e-3 * s ^ 2 + 0.2045196327e-3 * sqrt(0.2e1) * s + 0.4494937116e-4) * (0.1134002506e-1 * s + 0.7031536766e-2 * sqrt(0.2e1)) * (0.3410e1 * s + 0.3899423034e-1 * sqrt(0.2e1)) * (0.1041400392e0 * s + 0.2130846244e-1 * sqrt(0.2e1)) * alpha -0.3637001775e0 * sqrt(0.2e1) / (s + beta) / (0.2479996376e-3 * s ^ 2 + 0.2045196327e-3 * sqrt(0.2e1) * s + 0.4494937116e-4) * (0.1041400392e0 * s + 0.2130846244e-1 * sqrt(0.2e1)) * (0.3190e1 * s + 0.3771538148e-1 * sqrt(0.2e1)) * beta; -0.1374739512e1 * sqrt(0.2e1) / (s + alpha) / (0.2479996376e-3 * s ^ 2 + 0.2045196327e-3 * sqrt(0.2e1) * s + 0.4494937116e-4) * (0.1134002506e-1 * s + 0.7031536766e-2 * sqrt(0.2e1)) * (0.3410e1 * s + 0.3899423034e-1 * sqrt(0.2e1)) * alpha 0.1034482758e3 / (s + beta) / (0.2479996376e-3 * s ^ 2 + 0.2045196327e-3 * sqrt(0.2e1) * s + 0.4494937116e-4) * (0.1134002506e-1 * s + 0.7031536766e-2 * sqrt(0.2e1)) * (0.1041400392e0 * s + 0.2130846244e-1 * sqrt(0.2e1)) * (0.3190e1 * s + 0.3771538148e-1 * sqrt(0.2e1)) * beta;];
%Q = [0.1129032258e3 / (0.2479996376e-3 * s ^ 2 + 0.2045196327e-3 * sqrt(0.2e1) * s + 0.4494937116e-4) * (0.1041400392e0 * s + 0.2130846244e-1 * sqrt(0.2e1)) * (0.1134002506e-1 * s + 0.7031536766e-2 * sqrt(0.2e1)) * (0.3410e1 * s + 0.3899423034e-1 * sqrt(0.2e1)) * alpha / s -0.3637001775e0 * sqrt(0.2e1) / (0.2479996376e-3 * s ^ 2 + 0.2045196327e-3 * sqrt(0.2e1) * s + 0.4494937116e-4) * (0.1041400392e0 * s + 0.2130846244e-1 * sqrt(0.2e1)) * (0.3190e1 * s + 0.3771538148e-1 * sqrt(0.2e1)) * beta / s; -0.1374739512e1 * sqrt(0.2e1) / (0.2479996376e-3 * s ^ 2 + 0.2045196327e-3 * sqrt(0.2e1) * s + 0.4494937116e-4) * (0.1134002506e-1 * s + 0.7031536766e-2 * sqrt(0.2e1)) * (0.3410e1 * s + 0.3899423034e-1 * sqrt(0.2e1)) * alpha / s 0.1034482758e3 / (0.2479996376e-3 * s ^ 2 + 0.2045196327e-3 * sqrt(0.2e1) * s + 0.4494937116e-4) * (0.1041400392e0 * s + 0.2130846244e-1 * sqrt(0.2e1)) * (0.3190e1 * s + 0.3771538148e-1 * sqrt(0.2e1)) * (0.1134002506e-1 * s + 0.7031536766e-2 * sqrt(0.2e1)) * beta / s;]

ksi1 = 1;
ksi2 = 1;
w1 = 0.07;
w2 = 0.06;
Q = [0.1129032258e3 / (0.2479996376e-3 * s ^ 2 + 0.2045196327e-3 * sqrt(0.2e1) * s + 0.4494937116e-4) * (0.1041400392e0 * s + 0.2130846244e-1 * sqrt(0.2e1)) * (0.1134002506e-1 * s + 0.7031536766e-2 * sqrt(0.2e1)) * (0.3410e1 * s + 0.3899423034e-1 * sqrt(0.2e1)) * (w1 ^ 2) / s / (s + (2 * ksi1 * w1)) -0.3637001775e0 * sqrt(0.2e1) / (0.2479996376e-3 * s ^ 2 + 0.2045196327e-3 * sqrt(0.2e1) * s + 0.4494937116e-4) * (0.1041400392e0 * s + 0.2130846244e-1 * sqrt(0.2e1)) * (0.3190e1 * s + 0.3771538148e-1 * sqrt(0.2e1)) * (w2 ^ 2) / s / (s + (2 * ksi2 * w2)); -0.1374739512e1 * sqrt(0.2e1) / (0.2479996376e-3 * s ^ 2 + 0.2045196327e-3 * sqrt(0.2e1) * s + 0.4494937116e-4) * (0.1134002506e-1 * s + 0.7031536766e-2 * sqrt(0.2e1)) * (0.3410e1 * s + 0.3899423034e-1 * sqrt(0.2e1)) * (w1 ^ 2) / s / (s + (2 * ksi1 * w1)) 0.1034482758e3 / (0.2479996376e-3 * s ^ 2 + 0.2045196327e-3 * sqrt(0.2e1) * s + 0.4494937116e-4) * (0.1041400392e0 * s + 0.2130846244e-1 * sqrt(0.2e1)) * (0.3190e1 * s + 0.3771538148e-1 * sqrt(0.2e1)) * (0.1134002506e-1 * s + 0.7031536766e-2 * sqrt(0.2e1)) * (w2 ^ 2) / s / (s + (2 * ksi2 * w2));]

K_pruh = ksi_t*K;
K_lepsi = minreal(K_pruh) %final regulator

%{
%jeste porovnat s Q-param
hladina_1 = 0.1;
hladina_2 = 0.2;
sim('Semestralka_5_2')
figure(1)
hold on
plot(sys_T.time(:),sys_T.signals.values(:,1),'r')
plot(sys_T.time(:),sys_T.signals.values(:,2),'b')
plot(sys_T_star.time(:),sys_T_star.signals.values(:,1),'g--')
plot(sys_T_star.time(:),sys_T_star.signals.values(:,2),'k--')
plot(sys_Q.time(:),sys_Q.signals.values(:,1),'c')
plot(sys_Q.time(:),sys_Q.signals.values(:,2),'m')
legend('T - hladina 1','T - hladina 2','T^* - hladina 1','T^* - hladina 2','Q-param - hladina 1','Q-param - hladina 2')
%title('Porovnani vysledneho vystupu s pozadovanym vystupem systemu T^*')
xlabel('Cas [t]')
ylabel('Hladiny [m]')
hold off
%}


%==========================================================================
%3 ukol
%--------------------------------------------------------------------------
%{
M = 2;
A = 1e-4;
wb = 5e-2;
%}

M = 0.1;
A = 1e-4;
wb = 1e-6;

%w2 = 1;
w1 = (((s/M)+wb))/((s+wb*A));
w2 = s*(s^2+0.1*s+1)/(s^3 *(s +1));
W1 = w1*eye(2);
W2 = w2*eye(2);


G = Fs;
%{
P_new = [W1, W1*G;zeros(2,2),-W2;-eye(2),-G];

P1_new = ss(P_new);
P2_new = minreal(P1_new);
P3_new = mktito(P2_new,2,2);

P1_new = augw(G,W1,W2)
P_new = mktito(P1_new)
[K,CL,GAM,INFO] = mixsyn(P_new)   %H nekonecno norma

S = inv(eye(2) + G*K);
T = eye(2)-S;
Ks = K*S;
%}

P_new = [W1, W1*G;zeros(2,2),-W2;-eye(2),-G];

P1_new = ss(P_new);
%P2_new = minreal(P1_new);
P3_new = mktito(P1_new,2,2);


S = inv(eye(2) + G*K);
T = eye(2)-S;
[K, CL, Gam, Info] = h2syn(P3_new);   %h2 norma
Ks = K;
K_lepsi = Ks;

%singularni cisla
%%{
figure(2)
hold on
sigma(S,'r',Ks,'b',K,'g',inv(W1),'--r',ss(inv(W2)),'--b',T,'k') %singularni cisla
grid;
legend('S','Ks','K','inv W1','inv W2','T')
hold off

%}

hladina_1 = 0.1;
hladina_2 = 0.2;
sim('Semestralka_5_2')
figure(3)
hold on
plot(sys_T.time(:),sys_T.signals.values(:,1),'r')
plot(sys_T.time(:),sys_T.signals.values(:,2),'b')
plot(sys_T_star.time(:),sys_T_star.signals.values(:,1),'g--')
plot(sys_T_star.time(:),sys_T_star.signals.values(:,2),'k--')
plot(sys_Q.time(:),sys_Q.signals.values(:,1),'c')
plot(sys_Q.time(:),sys_Q.signals.values(:,2),'m')
legend('T - hladina 1','T - hladina 2','T^* - hladina 1','T^* - hladina 2','Q-param - hladina 1','Q-param - hladina 2')
%title('Porovnani vysledneho vystupu s pozadovanym vystupem systemu T^*')
xlabel('Cas [t]')
ylabel('Hladiny [m]')
hold off


%{

M = 8.1;
A = 0.1;
wb = 2e-3;

w1 = ((s/M)+wb)/(s+wb*A);
W1 = w1*eye(2);
%c = 1e-4;
c = 1.3e-1;
W2 = c*eye(2);
G = Fs;
P_new = [W1, W1*G;zeros(2,2),-W2;-eye(2),-G];

P1_new = ss(P_new);
P2_new = minreal(P1_new);
P3_new = mktito(P2_new,2,2);
[K,CL,GAM,INFO] = hinfsyn(P3_new)   %H nekonecno norma

S = inv(eye(2) + G*K);
T = eye(2)-S;
Ks = K*S;

%singularni cisla
figure(2)
hold on
sigma(S,'r',Ks,'b',K,'g',inv(W1),'--r',ss(inv(W2)),'--b',T,'k') %singularni cisla
grid;
legend('S','Ks','K','inv W1','inv W2','T')
hold off




%}



%{


sim('Semestralka_5_3_bez_poruchy_podminky')
regulace_1

figure(3)
hold on
plot(sys_T.time(:),sys_T.signals.values(:,1),'r')
plot(sys_T.time(:),sys_T.signals.values(:,2),'b')
plot(sys_T_star.time(:),sys_T_star.signals.values(:,1),'g--')
plot(sys_T_star.time(:),sys_T_star.signals.values(:,2),'k--')
legend('T - hladina 1','T - hladina 2','T^* - hladina 1','T^* - hladina 2')
%title('Porovnani vysledneho vystupu s pozadovanym vystupem systemu T^*')
xlabel('Cas [t]')
ylabel('Hladiny [m]')
hold off


Semestralka_5_3 - porucha

%}



%K - reg
%S - sys
%Ks - reg*sys
%makeweight - vyrabi ty vahy?





%{
%3 ukol
%--------------------------------------------------------------------------
M = 1.5;
A = 1e-3;
wb = 5e-2;

W1 = w1*[150,0;0,1];
%c = 1e-4;
c = 1e-2;
W2 = c*[1,0;0,0.1];
G = Fs;
P_new = [W1, W1*G;zeros(2,2),-W2;-eye(2),-G];

P1_new = ss(P_new);
P2_new = minreal(P1_new);
P3_new = mktito(P2_new,2,2);
[K,CL,GAM,INFO] = hinfsyn(P3_new)   %H nekonecno norma

S = inv(eye(2) + G*K);
T = eye(2)-S;
Ks = K*S;



%}




















