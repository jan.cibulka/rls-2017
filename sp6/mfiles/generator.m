%RLS SP6 - VYTVORENI MODELU VYSTUPNI MULTIPLIKATIVNI NEURCITOSTI
%DOPLNTE KOD NA VHODNEM MISTE

clear all; 
close all; clc;

%perturbace hladiny
h10_l=0.2;
h10_h=3;
h20_l=0.2;
h20_h=3;

ns=200; %pocet nahodnych vzorku pro  vytvoreni mnozinoveho modelu

h10_rand = h10_l + (h10_h-h10_l).*rand(ns,1);
h20_rand = h20_l + (h20_h-h20_l).*rand(ns,1);

s=zpk('s');

%nominalni system

h10=1.5;
h20=1.2;
[c1,c2,c3,c4,T1n,T2n,T3n,T4n]=Nadoby_rand(h10,h20);

P11=c1*T1n/(T1n*s+1);
P12=c2*T1n/((T1n*s+1)*(T3n*s+1));
P21=c3*T2n/((T2n*s+1)*(T4n*s+1));
P22=c4*T2n/(T2n*s+1);
     
Pn=[P11 P12;P21 P22];

T1=zeros(1,ns);
T2=zeros(1,ns);
T3=zeros(1,ns);
T4=zeros(1,ns);

%w=linspace(1e-3,500,2000); %frekvence pro vysetreni multiplik. perturbace
w=logspace(-3,3,2000);

%naplneni vektoru parametru
figure;

for i=1:ns
    [c1,c2,c3,c4,T1(i),T2(i),T3(i),T4(i)]=Nadoby_rand(h10_rand(i),h20_rand(i));
    
    P11=c1*T1(i)/(T1(i)*s+1);
    P12=c2*T1(i)/((T1(i)*s+1)*(T3(i)*s+1));
    P21=c3*T2(i)/((T2(i)*s+1)*(T4(i)*s+1));
    P22=c4*T2(i)/(T2(i)*s+1);
    
end;

r0 = zeros(1,length(w));

for k=1:length(w)
    %Nominalni model
    P11n=c1*T1n/(T1n*(1i*w(k))+1);
    P12n=c2*T1n/((T1n*(1i*w(k))+1)*(T3n*(1i*w(k))+1));
    P21n=c3*T2n/((T2n*(1i*w(k))+1)*(T4n*(1i*w(k))+1));
    P22n=c4*T2n/(T2n*(1i*w(k))+1);
    G=[P11n P12n;P21n P22n];
    
    sig=zeros(1,ns);
    for i=1:ns
        P11=c1*T1(i)/(T1(i)*(1i*w(k))+1);
        P12=c2*T1(i)/((T1(i)*(1i*w(k))+1)*(T3(i)*(1i*w(k))+1));
        P21=c3*T2(i)/((T2(i)*(1i*w(k))+1)*(T4(i)*(1i*w(k))+1));
        P22=c4*T2(i)/(T2(i)*(1i*w(k))+1);
        
        Gp=[P11 P12;P21 P22];
   
        sig(1,i) = norm((Gp-G)/(G));        
    end;
       
    r0(k) = max(sig);   
end;    

semilogx(w,r0); % relativni mira neurcitosti
title('Odhad w_O');
hold off;

save('r0_gen','r0');
