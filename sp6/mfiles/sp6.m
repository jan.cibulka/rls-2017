%% sesta semestralni prace
clearvars;
close all;
clc;

%perturbace hladiny
h10_l=0.2;
h10_h=3;
h20_l=0.2;
h20_h=3;

ns=20; %pocet nahodnych vzorku pro vytvoreni mnozinoveho modelu

h10_rand = h10_l + (h10_h-h10_l).*rand(ns,1);
h20_rand = h20_l + (h20_h-h20_l).*rand(ns,1);

T1=zeros(1,ns);
T2=zeros(1,ns);
T3=zeros(1,ns);
T4=zeros(1,ns);

%% 1
%nominalni system
h10 = 1.2;
h20 = 1.5;

w=logspace(-3,3,2000);
load('r0_gen.mat'); 
% nebo
%generator %generator perturbovanych systemu

figure;
semilogx(w,r0);
grid on;
title('Odhad w_O');

%% 2
% KPI:
% NS -> Nominal stability
% NP -> Nominal performace
% RS -> Robust stability
% RP -> Robust performace
load('model.mat'); % -> K, Pn

%NS
S = 1/((eye(2) + Pn*K));
poles = pole(S);
figure;
plot(real(poles),imag(poles),'o');
grid on;
xlim([-2 1]);
title('Poly citlivostni funkce S');

%NP
s = zpk('s');
w1 = (0.5*(s+0.02))/(s+1e-6);
W1 = w1*eye(2);

figure;
sigma(W1*S);
grid on;
legend('W1S');

figure;
sigma(S, inv(w1));
grid on;
legend('S', 'W1^{-1}');


%% 3
%RS
T = eye(2)-S;
sigRS = sigma(w,T);
rs = r0 .* sigRS(1,:);

figure;
plot(rs);
grid on;
title('Podminka robustni stability');

% 20 realizaci
figure;
for i=1:ns
    [c1,c2,c3,c4,T1(i),T2(i),T3(i),T4(i)]=Nadoby_rand(h10_rand(i),h20_rand(i));    
    
    P11=c1*T1(i)/(T1(i)*s+1);
    P12=c2*T1(i)/((T1(i)*s+1)*(T3(i)*s+1));
    P21=c3*T2(i)/((T2(i)*s+1)*(T4(i)*s+1));
    P22=c4*T2(i)/(T2(i)*s+1);
      
    P=[P11 P12;P21 P22];
    
    Spom = inv(eye(2)+P*K);
    Tpom = eye(2) - Spom;
    
    sig = max(sigma(Tpom,w));
    RS=zeros(1,length(r0));

    for j = 1:1:length(r0)
         RS(j) = r0(j)*sig(j);
    end
    MI = max(sigma(W1*S,w)) + RS;
    plot(MI); %ukol 4 podminka RP
    %plot(RS) %ukol 3 podminka RS
    hold on;     
end;
grid on;
title('Simulacni overeni podminky nahodnym generovanim 20 systemu');

%% 4
%RP
% ||W1S(delta)||inf <1)
mi = max(sigma(w1*S,w)) + rs;

%RP neni splnena -> mus�m navrhnout robustni regulatoru resenim ST probblemu

figure;
plot(mi);
ylim([0.5 1.4]);
grid on;
title('Podminka robustni kvality rizeni');

%% 5
% ||H|| = ||W1s ; W0T|| -> min
[K2,CL,GAM,INFO]=mixsyn(Pn,w1,[],0.7);

% loops = loopsens(Pn,K2); 
% %bode(loops.Si,'r',loops.Ti,'b',loops.Li,'g')
% figure;
% hold on;
% grid on;
% sigma(K2);
% sigma(inv(w1));
% sigma(loops.Ti);
% sigma(loops.Si);
% legend('K','inv w1','T','S');

S2pom = inv(eye(2)+Pn*K2);
T2pom = eye(2) - S2pom;

% RS
sig2 = max(sigma(T2pom,w));
RS2=zeros(1,length(r0));

for i = 1:1:length(r0)
    RS2(i) = 0.7*sig2(i);
end

% 20 realizaci
figure;
for i=1:ns
    [c1,c2,c3,c4,T1(i),T2(i),T3(i),T4(i)]=Nadoby_rand(h10_rand(i),h20_rand(i));
    P11=c1*T1(i)/(T1(i)*s+1);
    P12=c2*T1(i)/((T1(i)*s+1)*(T3(i)*s+1));
    P21=c3*T2(i)/((T2(i)*s+1)*(T4(i)*s+1));
    P22=c4*T2(i)/(T2(i)*s+1);
      
    P=[P11 P12;P21 P22];
    
    S2pom = 1/(eye(2)+P*K2);
    T2pom = eye(2) - S2pom;
    
    RS2=zeros(1,length(r0));

    for j = 1:1:length(r0)
         RS2(j) = 0.7*sig2(j);
    end
    MI2 = RS2 + max(sigma(W1*S2pom,w));
    %plot(RS2);
    plot(MI2);
    hold on;
     
end;
grid on;
%title('Simulacni overeni podminky RS nahodnym generovanim 20 systemu');
title('Simulacni overeni podminky RP nahodnym generovanim 20 systemu');

figure;
plot(RS2);
grid on;
%title('Podminka RS');
title('Podminka RP');



%% 6

Kn1 = ureal('Kn1',1,'Percentage',50); % <0.5, 1.5>
Kn2 = ureal('Kn2',1,'Percentage',50); % <0.5, 1.5>

taun1 = ureal('taun1',2,'Range',[1.5 2.5]); % <1.5, 2.5>
taun2 = ureal('taun2',2,'Range',[1.5 2.5]); % <1.5, 2.5>

Pu = [Kn1/(taun1*s + 1) 0 ;0 Kn2/(taun2*s + 1)];
P = Pn * Pu;

%L = P*K2;
L = P*K;
S = (inv(eye(2) + L));

figure;
sigma(S, inv(w1));
grid on;
legend('S', 'W1^{-1}');

rs = robstab(w1*S);
[wcg,wcu] = wcgain(w1*S);


%% 7
Ppom =  -[W1; eye(2)]*[eye(2) P];

[kz,clp,bnd] = dksyn(Ppom,2,2); %bnd je mensi nez 1 -> hura

figure;
hsvd(kz);
grid on;

% redukce radu regulatoru
k = minreal(kz);
k = balred(k,8); 

figure
hold on;
sigma(kz);
sigma(k);
grid on;
legend('K','K_{redukovany}')

S7 = 1/(eye(2)+P*k);
figure;
sigma(W1*S7);
grid on;

[wcg2,wcu2] = wcgain(W1*S7);
